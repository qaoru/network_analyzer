/**
 * @file dhcp.c
 * @author Paul HENG (paul.heng@etu.unistra.fr)
 * @brief dhcp parser
 * @version 0.1
 * @date 2019-10-21
 * 
 * @copyright Copyright (c) 2019
 * 
 */
#include "proto/dhcp.h"

void handle_bootp(struct bootp *bootstrap, uint16_t length) {
        uint16_t flags = ntohs(bootstrap->bp_flags);
        printf("length: %hu, ", length);
        printf("%s",
            bootstrap->bp_op == BOOTREQUEST ? "Request" :
            bootstrap->bp_op == BOOTREPLY ? "Reply" :
            "Unknown Operation");
        
        if (vflag == V_FULL) {

        printf("Hardware type: 0x%.2X %s\n",
            bootstrap->bp_htype,
            bootstrap->bp_htype == 0x01 ? "(Ethernet)" : ""); //? todo
        printf("Hardware address length: %u\n", bootstrap->bp_hlen);
        printf("Hops: %u\n", bootstrap->bp_hops);
        printf("Transaction ID: 0x%x\n", ntohl(bootstrap->bp_xid));
        printf("Seconds elapsed: %hu\n", ntohs(bootstrap->bp_secs));
        printf("Flags: 0x%.4x (%s)\n", flags, flags ? "Unicast" : "Broadcast");
        printf("Client IP address: %s\n", inet_ntoa(bootstrap->bp_ciaddr));
        printf("Your IP address: %s\n", inet_ntoa(bootstrap->bp_yiaddr));
        printf("Next server IP address: %s\n", inet_ntoa(bootstrap->bp_siaddr));
        printf("Relay agent IP address: %s\n", inet_ntoa(bootstrap->bp_giaddr));
        printf("Client MAC address: %s\n", get_mac_addr(bootstrap->bp_chaddr));
        printf("Server host name: %s\n", bootstrap->bp_sname);
        printf("Boot file name: %s\n", bootstrap->bp_file);
    // cprintf(V_MEDIUM, "Magic cookie: %s\n", bootstrap->bp_vend);
    } else {
        if (bootstrap->bp_op == BOOTREPLY) {
            printf(" %s to %s", inet_ntoa(bootstrap->bp_yiaddr), get_mac_addr(bootstrap->bp_chaddr));
        } else {
            printf(" from %s", get_mac_addr(bootstrap->bp_chaddr));
        }
    }
    parse_tlv_bootp(bootstrap->bp_vend);
}

void parse_tlv_bootp(u_int8_t *vend) {
    static const char *dhcp_msg2str[] = {
        "\0", // unused
        "Discover",
        "Offer",
        "Request",
        "Decline",
        "ACK",
        "NAK",
        "Release",
        "Inform"
    };

    u_int8_t magic_cookie[4] = VM_RFC1048;
    if (vend[0] != magic_cookie[0] ||
        vend[1] != magic_cookie[1] ||
        vend[2] != magic_cookie[2] ||
        vend[3] != magic_cookie[3]) {
        return; // no magic cookie
    }
    printf(", Magic Cookie present (DHCP)\n");
    int i = 4;
    uint8_t type, length;
    if (vflag == V_MEDIUM) {
        printf("(");
    }
    while (vend[i] != 0xff) {
        type = vend[i++];
        cprintf(V_FULL, CLS_WHITE_B"Option <%d>\t"CLS_RESET, type);
        length = vend[i];
        cprintf(V_FULL, "(Length: %d)\t=>\t", length);
        if (vflag == V_FULL) {
            switch(type) {
                case TAG_DHCP_MESSAGE:
                    printf("DHCP Message Type (%d - %s)\n",
                        vend[i + length],
                        dhcp_msg2str[vend[i + length]]);
                    break;
                case TAG_SERVER_ID:
                    printf("DHCP Server Identifier: %s\n",
                    tlv_getip(vend + i + 1, length));
                    break;
                case TAG_IP_LEASE:
                    printf("IP Lease Time: %lus\n",
                    tlv_getint(vend + i + 1, length));
                    break;
                case TAG_RENEWAL_TIME:
                    printf("Renewal Time Value: %lus\n",
                    tlv_getint(vend + i + 1, length));
                    break;
                case TAG_REBIND_TIME:
                    printf("Rebinding Tme Value: %lus\n",
                    tlv_getint(vend + i + 1, length));
                    break;
                case TAG_SUBNET_MASK:
                    printf("Subnet Mask: %s\n",
                    tlv_getip(vend + i + 1, length));
                    break;
                case TAG_DOMAIN_SERVER:
                    printf("Domain Name Server (DNS): %s\n",
                    tlv_getip(vend + i + 1, length));
                    break;
                case TAG_DOMAINNAME:
                    printf("Domain Name: %s\n",
                    tlv_getstring(vend + i, length));
                    break;
                case TAG_GATEWAY:
                    printf("Default Gateway: %s\n",
                    tlv_getip(vend + i + 1, length));
                    break;
                case TAG_MAX_MSG_SIZE:
                    printf("Maximum DHCP Message Size: %lu\n",
                    tlv_getint(vend + i + 1, length));
                    break;
                case TAG_CLIENT_ID:
                    printf("Client Identifier: ");
                    if (vend[i+1] == 0x01) { // ethernet
                        printf("%s\n",
                        tlv_getmac(vend + i + 2));
                    } else {
                        printf("Hardware Type 0x%.2x not implemented.\n",
                        vend[i+1]);
                    }
                    break;
                case TAG_REQUESTED_IP:
                    printf("Requested IP Address: %s\n",
                    tlv_getip(vend + i + 1, length));
                    break;
                case TAG_HOSTNAME:
                    printf("Host Name: %s\n",
                    tlv_getstring(vend + i, length));
                    break;
                default:
                    printf("Not implemented in this program.\n");
                    break;
            }
        } else if (vflag == V_MEDIUM) {
            switch (type) {
                case TAG_REQUESTED_IP:
                    printf("req IP: %s\t",
                    tlv_getip(vend + i + 1, length));
                    break;
                case TAG_GATEWAY:
                    printf("GW: %s\t",
                    tlv_getip(vend + i + 1, length));
                    break;
                case TAG_SUBNET_MASK:
                    printf("mask: %s\t",
                    tlv_getip(vend + i + 1, length));
                    break;
                default:
                    break;
            }
        }
        i += 1 + length;
        // printf("<%.2X, %d> ", vend[i], i);
    }
    if (vflag == V_MEDIUM) {
        printf(")");
    }
}