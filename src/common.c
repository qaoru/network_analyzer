/**
 * @file common.c
 * @author Paul HENG (paul.heng@etu.unistra.fr)
 * @brief common functions / utilities
 * @version 0.1
 * @date 2019-10-21
 * 
 * @copyright Copyright (c) 2019
 * 
 */
#include "common.h"

void error (int syserr, const char *fmt, ...) {
    va_list ap;
    va_start (ap, fmt);
    fprintf (stderr, "%s: ", prog);
    vfprintf (stderr, fmt, ap);
    fprintf (stderr, "\n");
    va_end (ap);
    if (syserr)
	    perror ("");
    exit (1);
}

void cprintf (int threshold, const char *fmt, ...) {
    if (vflag < threshold) { return; }
    va_list ap;
    va_start (ap, fmt);
    vfprintf (stdout, fmt, ap);
    fflush(stdout);
    va_end (ap) ;
}

char *get_timestamp(struct timeval tv) {
    static char buf[64];
    time_t _time;
    struct tm *_tm;
    char tmbuf[64];

    _time = tv.tv_sec;
    _tm = localtime(&_time);
    strftime(tmbuf, sizeof tmbuf, "%Y-%m-%d %H:%M:%S", _tm);
    snprintf(buf, BUFSIZE, "%s", tmbuf);

    return buf;
}

void putsep(void) {
    if (vflag == V_FULL) {
        putchar('\n');
    } else {
        putchar('\t');
    }
}

void putsep_inline(void) {
    if (vflag == V_FULL) {
        putchar('\t');
    } else {
        putchar(' ');
    }
}