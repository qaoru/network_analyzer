/**
 * @file ethertools.c
 * @author Paul HENG (paul.heng@etu.unistra.fr)
 * @brief Ethernet Layer related tools
 * @version 0.1
 * @date 2019-10-21
 * 
 * @copyright Copyright (c) 2019
 * 
 */
#include "ethertools.h"
#include "iptools.h"

char *get_mac_addr(const u_char host[6]) {
    static char buf[ETH_STR_SIZE];
    if (
        snprintf(
            buf,
            ETH_STR_SIZE,
            "%.02x:%.02x:%.02x:%.02x:%.02x:%.02x",
            host[0], host[1], host[2], host[3], host[4], host[5]
        ) < 0
    ) {
        error(0, "snprintf\n");
    }
    return buf;
}

char *get_ethertype_name(const uint16_t ethertype) {
    static char buf[BUFSIZE];

    switch (ethertype) {
        case ETHERTYPE_IP:
            strcpy(buf, "IPv4");
            break;
        case ETHERTYPE_IPV6:
            strcpy(buf, "IPv6");
            break;
        case ETHERTYPE_ARP:
            strcpy(buf, "ARP");
            break;
        default:
            strcpy(buf, "Unknown");
            break;
    }
    return buf;
}

void print_ethernet_info(const u_char *packet, unsigned int length) {
    const struct ether_header *ethernet;
    ethernet = (struct ether_header *) packet;
    printf("%s", get_mac_addr(ethernet->ether_shost));
    printf(" > %s ", get_mac_addr(ethernet->ether_dhost));
    printf("EtherType %s (0x%04x), length %hu",
        get_ethertype_name(ntohs(ethernet->ether_type)),
        ntohs(ethernet->ether_type),
        length);
    if (vflag == V_SHORT) {
        printf(": ");
    } else {
        putchar('\n');
    }

    
    switch (ntohs(ethernet->ether_type))
    {
    case ETHERTYPE_IP:
        print_ip_info(packet + ETH_SIZE);
        break;
    case ETHERTYPE_IPV6:
        print_ip6_info(packet + ETH_SIZE);
        break;
    case ETHERTYPE_ARP:
        print_arp(packet + ETH_SIZE);
        break;
    default:
        break;
    }
}