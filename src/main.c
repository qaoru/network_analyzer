/**
 * @file main.c
 * @author Paul HENG (paul.heng@etu.unistra.fr)
 * @brief main file
 * @version 0.1
 * @date 2019-10-21
 * 
 * @copyright Copyright (c) 2019
 * 
 */
#include <stdio.h>
#include <pcap.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include "ethertools.h"
#include "iptools.h"

void print_packet_info(const u_char *packet, struct pcap_pkthdr packet_header) {
    printf("%s ", get_timestamp(packet_header.ts));
    print_ethernet_info(packet, packet_header.len);

}



void print_packet(const u_char *packet, struct pcap_pkthdr packet_header) {
    unsigned int i;
    for (i = 0; i < packet_header.caplen; i++) {
        cprintf(V_FULL, "%.2X ", packet[i]);
    }
}

void handler (
    u_char *args,
    const struct pcap_pkthdr *packet_header,
    const u_char *packet_body
) {
    (void) args;
    // printf("---\n");
    print_packet_info(packet_body, *packet_header);
    //print_packet(packet_body, *packet_header);
    printf("\n\n");
    // printf("---\n");
    return;
}

int main(int argc, char *argv[]) {
    // init
    prog = argv[0];
    vflag = V_SHORT;

    // options
    int opt;
    int mode = 0; // 1: live, 2: offline
    char optstring[] = "i:f:v:o:";
    char path[BUFSIZE];
    char filter_expr[BUFSIZE];
    char dev_if[BUFSIZE];
    memset(path, 0, BUFSIZE);
    int flt = 0;

    while ((opt = getopt(argc, argv, optstring)) != EOF) {
        switch(opt) {
            case 'i':
                mode += 1;
                strcpy(dev_if, optarg);
                break;
            case 'o':
                mode += 2;
                strcpy(path, optarg);
                break;
            case 'v':
                vflag = atoi(optarg);
                break;
            case 'f':
                flt = 1;
                strcpy(filter_expr, optarg);
                break;
        }
    }

    if (mode >= 3) {
        error(0, "-i and -f options are incompatible\n");
    }

    // PCAP vars
    char *device;
    char errbuf[PCAP_ERRBUF_SIZE];
    pcap_t *handle;
    int timeout_limit = 10000; //* ms
    bpf_u_int32 dev_ip, dev_mask;
    struct bpf_program filter;

    if (mode == 1) {
        device = dev_if;
    } else {
        device = pcap_lookupdev(errbuf);
        if (device == NULL) {
            error(0, "Device not found: %s", errbuf);
        }
    }

    // Device Info

    if (pcap_lookupnet(device, &dev_ip, &dev_mask, errbuf) == -1) {
        fprintf(stderr, "lookupnet: %s\n", errbuf);
        exit(1);
    }

    struct in_addr dev_addr, subnet_addr;
    dev_addr.s_addr = dev_ip;
    subnet_addr.s_addr = dev_mask;


    if (mode <= 1) {
        printf("Selected interface: %s\n", device);
        printf("Current IP / Mask: %s / ", inet_ntoa(dev_addr));
        printf("%s\n", inet_ntoa(subnet_addr));
        /* Open device for live capture */
        handle = pcap_open_live(
                device,
                1500,
                1,
                timeout_limit,
                errbuf
            );

        if (handle == NULL) {
             error (0, "Could not open device %s: %s\n", device, errbuf);
        }

        if (flt) {
            if (pcap_compile(handle, &filter, filter_expr, 0, dev_ip) == -1) {
                error(0, "Bad pcap filter: %s\n", pcap_geterr(handle));
            }
            if (pcap_setfilter(handle, &filter) == -1) {
                error(0, "Error setting pcap filter: %s\n", pcap_geterr(handle));
            }
        }


    } else if (mode == 2) {
        handle = pcap_open_offline(path, errbuf);
        if (handle == NULL) {
            error(0, "Could not open pcap file %s: %s\n", path, errbuf);
        }
        printf("Opened pcap file: %s\n\n", path);
    }
    // Main Loop
    pcap_loop(handle, 0, handler, NULL);



    return 0;
}