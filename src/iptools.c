/**
 * @file iptools.c
 * @author Paul HENG (paul.heng@etu.unistra.fr)
 * @brief contains IP layer related functions + some other utilities
 * @version 0.1
 * @date 2019-10-21
 * 
 * @copyright Copyright (c) 2019
 * 
 */
#include "iptools.h"
#include "proto/dhcp.h"
#include "proto/dns.h"

u_int64_t tlv_getint(const u_char *packet, int length) {
    u_int64_t buf = 0;
    int j;
    for (j = 1; j <= length; j++) { // load value into buffer
        buf |= packet[j-1] << (8 * (length - j));
    }
    return buf;
}

char *tlv_getstring(const u_char *packet, int length) {
    static char buf[1500];
    memset(buf, 0, 1500);
    int i = 0, j = 0;
    for (j = 1; j <= length; j++) {
        buf[i++] = packet[j];
    }
    buf[i] = '\0';
    return buf;
}

char *tlv_getip(const u_char *packet, int length) {
    static char buf[INET6_ADDRSTRLEN];
    memset(buf, 0, INET6_ADDRSTRLEN);
    if (length == 4) { // IPv4
        if ((snprintf(buf, INET_ADDRSTRLEN, "%d.%d.%d.%d",
            packet[0], packet[1],packet[2], packet[3])) < 0) {
            error(0, "snprintf");
        }
    } else if (length == 16) { // IPv6
        if ((snprintf(buf, INET6_ADDRSTRLEN,
        "%.4X:%.4X.%.4X:%.4X:%.4X:%.4X.%.4X:%.4X",
            packet[0], packet[1],packet[2], packet[3],
            packet[4], packet[5],packet[6], packet[7])) < 0) {
            error(0, "snprintf");
        }
    }
    return buf;
}

char *tlv_getmac(const u_char *packet) {
    static char buf[ETH_STR_SIZE];
    if (
        snprintf(
            buf,
            ETH_STR_SIZE,
            "%.02x:%.02x:%.02x:%.02x:%.02x:%.02x",
            packet[1], packet[2], packet[3], packet[4], packet[5], packet[6]
        ) < 0
    ) {
        error(0, "snprintf\n");
    }
    return buf;
}

char *get_proto_name(int protocol) {
    static char name[BUFSIZE];
    switch (protocol) {
        case 0:
            sprintf(name, "IP6 Hop-By-Hop (%d)", protocol);
            break;
        case IPPROTO_ICMP:
            sprintf(name, "ICMP");
            break;
        case IPPROTO_ICMPV6:
            sprintf(name, "ICMPv6");
            break;
        case IPPROTO_TCP:
            sprintf(name, "TCP");
            break;
        case IPPROTO_UDP:
            sprintf(name, "UDP");
            break;
        default:
            return NULL;
    }
    return name;
}

char *check_fragment(uint16_t frag_off) {
    static char fragment_status[BUFSIZE];
    if (ntohs(frag_off) == 0x4000) {
        strcpy(fragment_status, "Don't fragment");
    }
    return fragment_status;
}

char *get_flags(uint16_t f) {
    static char buf[16];
    memset(buf, 0, 16);
    int rb, df, mf, offset = 0;
    rb = f >> 15 & 1;
    df = f >> 14 & 1;
    mf = f >> 13 & 1;
    if (rb) {
        buf[offset++] = 'R';
        if (df || mf) {
            buf[offset++] = ',';
        }
    }
    if (df) {
        buf[offset++] = 'D';
        buf[offset++] = 'F';
        if (mf) {
            buf[offset++] = ',';
        }
    }
    if (mf) {
        buf[offset++] = 'M';
        buf[offset++] = 'F';
    }
    return buf;
}

void print_ip_info(const u_char *packet) {
    const struct iphdr *ip;
    ip = (struct iphdr *) packet;
    struct in_addr s;

    if (vflag != V_SHORT) {
        printf(
            "(TOS 0x%x, TTL %d, ID %hu, offset %hu, flags [%s], "
            "proto %s (%hu), length %hu) ",
            ip->tos,
            ip->ttl,
            ntohs(ip->id),
            ntohs(ip->frag_off) & 0x1FFF,
            get_flags(ntohs(ip->frag_off)),
            get_proto_name(ip->protocol),
            ip->protocol,
            ntohs(ip->tot_len)
        );
    }

    s.s_addr = ip->saddr;
    printf("%s > ", inet_ntoa(s));
    s.s_addr = ip->daddr;
    printf("%s%c", inet_ntoa(s), vflag == V_SHORT ? '\t' : '\n');

    if (ip->protocol == IPPROTO_UDP) {
        parse_udp(packet + ip->ihl * 4);
    } else if (ip->protocol == IPPROTO_TCP) {
        parse_tcp(packet + ip->ihl * 4, ntohs(ip->tot_len) - ip->ihl * 4);
    }
}

void print_ip6_info(const u_char *packet) {
    const struct ip6_hdr * ip;
    ip = (struct ip6_hdr *) packet;
    struct in6_addr s;
    char buf[INET6_ADDRSTRLEN];
    int next_opt = ip->ip6_ctlun.ip6_un1.ip6_un1_nxt;
    u_char *ptr = (u_char *) packet;
    char *n = get_proto_name(next_opt);

    s = ip->ip6_src;
    printf("%s > ", inet_ntop(AF_INET6, &s, buf, INET6_ADDRSTRLEN));
    s = ip->ip6_dst;
    printf("%s ", inet_ntop(AF_INET6, &s, buf, INET6_ADDRSTRLEN));
    if (vflag > V_SHORT) {
        printf("(flow label %x, hlim %d, next-header %s, payload length %d)%c",
            ip->ip6_ctlun.ip6_un1.ip6_un1_flow,
            ip->ip6_ctlun.ip6_un1.ip6_un1_hlim,
            n,
            ip->ip6_ctlun.ip6_un1.ip6_un1_plen,
            TABSEP
        );
    }
    ptr += 40;
    while (next_opt != IPPROTO_TCP && next_opt != IPPROTO_UDP && n != NULL) {
        ptr += parse_ip6_opts(next_opt, ptr, &next_opt);
        n = get_proto_name(next_opt);
    }
    if (next_opt == IPPROTO_TCP) {
        parse_tcp(ptr, 0);
    } else if (next_opt == IPPROTO_UDP) {
        parse_udp(ptr);
    }
}


int parse_ip6_opts(int optnum, const u_char *packet, int *next) {
    struct ip6_hbh *hbh;
    switch (optnum)
    {
    case 0: // HOP-BY-HOP
        hbh = (struct ip6_hbh *) packet;
        printf("HBH: next header %s, length %d, ",
        get_proto_name(hbh->ip6h_nxt),
        hbh->ip6h_len + 8
        );
        *next = hbh->ip6h_nxt;
        return hbh->ip6h_len + 8;
    default:
        *next = -1;
        return 0;
    }
}

char *tcp_flags(const uint8_t flags) {
    static char buf[64];
    int off = 0;
    memset(buf, 0, 64);
    // if (flags & 0xE00) {
    //     buf[off++] = 'R';
    //     buf[off++] = 'E';
    //     buf[off++] = 'S';
    //     buf[off++] = '.';
    // }
    // if (flags & 0x100) {
    //     buf[off++] = 'N';
    //     buf[off++] = 'S';
    //     buf[off++] = '.';
    // }
    if (flags & 0x80) {
        buf[off++] = 'C';
        buf[off++] = 'W';
        buf[off++] = 'R';
        buf[off++] = '.';
    }
    if (flags & 0x40) {
        buf[off++] = 'E';
        buf[off++] = 'C';
        buf[off++] = 'E';
        buf[off++] = '.';
    }
    if (flags & TH_URG) {
        buf[off++] = 'U';
        buf[off++] = 'R';
        buf[off++] = 'G';
        buf[off++] = '.';
    }
    if (flags & TH_ACK) {
        buf[off++] = 'A';
        buf[off++] = 'C';
        buf[off++] = 'K';
        buf[off++] = '.';
    }
    if (flags & TH_PUSH) {
        buf[off++] = 'P';
        buf[off++] = 'S';
        buf[off++] = 'H';
        buf[off++] = '.';
    }
    if (flags & TH_RST) {
        buf[off++] = 'R';
        buf[off++] = 'S';
        buf[off++] = 'T';
        buf[off++] = '.';
    }
    if (flags & TH_SYN) {
        buf[off++] = 'S';
        buf[off++] = 'Y';
        buf[off++] = 'N';
        buf[off++] = '.';
    }
    if (flags & TH_FIN) {
        buf[off++] = 'F';
        buf[off++] = 'I';
        buf[off++] = 'N';
    }
    return buf;
}

char *tcp_opts(const u_char *ptr, int optlen) {
    static char buf[1024];
    int offset = 0, len, kind, k, cnt = 0;
    memset(buf, 0, BUFSIZE);
    while (cnt < optlen) {
        if (*ptr == 1) {
            offset += sprintf(buf + offset, "nop,");
            ptr++;
            cnt++;
            continue;
        }
        kind = *ptr;
        ptr++;
        len = *ptr - 2;
        ptr++;
        switch (kind)
        {
        case TCPOPT_MAXSEG:
            offset += sprintf(buf + offset, "MSS %hu,", (uint16_t) tlv_getint(ptr, len));
            break;
        case TCPOPT_WINDOW:
            offset += sprintf(buf + offset, "WS %d,", (u_int16_t) tlv_getint(ptr, len));
            break;
        case TCPOPT_SACK_PERMITTED:
            offset += sprintf(buf + offset, "SACK_P,");
            break;
        case TCPOPT_SACK:
            offset += sprintf(buf + offset, "SACK (");
            for (k = 0; k < len / 8; k += 8) {
                offset += sprintf(buf + offset, "%d:%d,",
                    (u_int32_t) tlv_getint(ptr + k, 4),
                    (u_int32_t) tlv_getint(ptr + k + 4, 4)
                );
            }
            buf[offset - 1] = ')';
            break;
        case TCPOPT_TIMESTAMP:
            offset += sprintf(buf + offset, "TS val %u ecr %u,",
                (u_int32_t) tlv_getint(ptr, 4),
                (u_int32_t) tlv_getint(ptr + 4, 4)
            );
            break;

        default:
            break;
        }
        ptr += len;
        cnt += 2 + (*ptr == 0 ? 0 : len); // end of option check
    }
    buf[offset - 1] = '\0';
    return buf;
}

void parse_tcp(const u_char *packet, uint16_t plen) {
    const struct tcphdr *tcp;
    tcp = (struct tcphdr *) packet;
    uint32_t tcp_plen = plen - tcp->th_off * 4;
    /**
     *  ? not sure whether to use tcp->th_d(s)port or tcp->dest(source)
     */
    uint16_t src = ntohs(tcp->th_sport);
    uint16_t dst = ntohs(tcp->th_dport);
    printf("sport = %hu > dport = %hu, flags [%s], ", src, dst,
    tcp_flags(tcp->th_flags));
    printf("seq %u, ack %u, win %hu, checksum 0x%x, payload length %d ",
        ntohl(tcp->th_seq),
        ntohl(tcp->th_ack),
        ntohs(tcp->th_win),
        ntohs(tcp->th_sum),
        tcp_plen
    );
    if (tcp->th_off * 4 > 20) {
        printf("[%s]", tcp_opts(packet + 20, tcp->th_off * 4 - 20));
    }
    // printf("len %hu", tcp->)
    putchar(TABSEP);
    if (tcp_plen) {
        print_application_layer(
            (src < 1024 ? src : dst),
            packet + tcp->th_off * 4,
            tcp_plen
        );
    }

}

void parse_udp(const u_char *packet) {
    const struct udphdr *udp;
    udp = (struct udphdr *) packet;
    uint16_t src = ntohs(udp->uh_sport);
    uint16_t dst = ntohs(udp->uh_dport);
    printf("sport = %hu > dport = %hu%c", src, dst, TABSEP);
    print_application_layer(
        (src < 1024 ? src : dst), 
    packet + sizeof(struct udphdr),
    ntohs(udp->uh_ulen) - sizeof(struct udphdr));
}

/**
 * @brief get ascii data from packet
 * returns only the first line if vflag is not set to V_FULL
 * 
 * @param packet 
 * @param length 
 * @return char* 
 */
char *get_ascii_data(const u_char *packet, uint16_t length) {
    static char buf[4096];
    int k;
    snprintf(buf, length, "%s", packet);
    k = length;
    if (vflag != V_FULL) {
        k = 0;
        while (buf[k] != '\n' && k < length) {
            k++;
        }
    }
    buf[k] = '\0';
    return buf;
}

void print_application_layer(uint16_t port, const u_char *packet, uint16_t length) {
    switch (port) {
        case 20:
            printf("FTP DATA %s\n", vflag == V_SHORT ? "" : get_ascii_data(packet, length));
            break;
        case 21:
            printf("FTP %s\n", vflag == V_SHORT ? "" : get_ascii_data(packet, length));
            break;
        case 22:
            printf("SSH\n");
            break;
        case 23:
            printf("Telnet\n");
            // if (vflag == V_FULL) {
            //     printf("%s\n", get_ascii_data(packet, length));
            // }
            break;
        case 25:
        case 465:
        case 587:
            printf("SMTP %s\n", vflag == V_SHORT ? "" : get_ascii_data(packet, length));
            break;
        case 53:
            printf("DNS, ");
            handle_dns((struct dnshdr *) packet);
            break;
        case 67:
            printf(CLS_YELLOW_B"BOOTP, "CLS_RESET);
            handle_bootp((struct bootp *) packet, length);
            break;
        case 68:
            printf(CLS_YELLOW_B"BOOTP, "CLS_RESET);
            handle_bootp((struct bootp *) packet, length);
            break;
        case 110:
            printf("POP %s\n", vflag == V_SHORT ? "" : get_ascii_data(packet, length));
            break;
        case 143:
        case 993:
            printf("IMAP %s\n", vflag == V_SHORT ? "" : get_ascii_data(packet, length));
            break;
        case 80:
            printf("HTTP %s\n", vflag == V_SHORT ? "" : get_ascii_data(packet, length));
            break;
        case 443:
            printf("HTTPS\n");
            break;
        case 389:
            printf("LDAP\n");
            break;
        default:
            printf("Unknown Protocol\n");
            break;
    }
}

/**
 * @brief returns a buffer containing the ARP operation name from its opcode
 * 
 * @param code 
 * @return char* 
 */
char *get_arp_op(u_short code) {
    static char buf[128];
    memset(buf, 0, 0);
    switch (code) {
        case ARPOP_REQUEST:
            strcpy(buf, "request");
            break;
        case ARPOP_REPLY:
            strcpy(buf, "reply");
            break;
        case ARPOP_RREQUEST:
            strcpy(buf, "RARP request");
            break;
        case ARPOP_RREPLY:
            strcpy(buf, "RARP reply");
            break;
        default: break;
    }
    return buf;
}


/**
 * @brief print arp informations
 * 
 * @param packet 
 */
void print_arp(const u_char *packet) {
    const struct arphdr *arp;
    arp = (struct arphdr *) packet;
    printf("%s, ", get_arp_op(ntohs(arp->ar_op)));
    if (vflag == V_SHORT) {
        printf("sender IP: %s, ", tlv_getip(packet + 14, 4));
        printf("target IP: %s", tlv_getip(packet + 24, 4));
    } else {
        printf("sender IP (MAC): %s (%s), ",
            tlv_getip(packet + 14, 4),
            tlv_getmac(packet + 8)
        );
        printf("target IP (MAC): %s (%s)",
            tlv_getip(packet + 24, 4),
            tlv_getmac(packet + 18)
        );
    }
}
