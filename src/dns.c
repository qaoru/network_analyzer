/**
 * @file dns.c
 * @author Paul HENG (paul.heng@etu.unistra.fr)
 * @brief dns parser
 * @version 0.1
 * @date 2019-10-21
 * 
 * @copyright Copyright (c) 2019
 * 
 */
#include "proto/dns.h"

uint8_t *orig;

void dns_print_type(uint16_t type) {
    switch(type) {
        case DNS_TYPE_A: printf("A"); break;
        case DNS_TYPE_NS: printf("NS"); break;
        case DNS_TYPE_MD: printf("MD"); break;
        case DNS_TYPE_MF: printf("MF"); break;
        case DNS_TYPE_CNAME: printf("CNAME"); break;
        case DNS_TYPE_SOA: printf("SOA"); break;
        case DNS_TYPE_MB: printf("MB"); break;
        case DNS_TYPE_MG: printf("MG"); break;
        case DNS_TYPE_MR: printf("MR"); break;
        case DNS_TYPE_NULL: printf("NULL"); break;
        case DNS_TYPE_WKS: printf("WKS"); break;
        case DNS_TYPE_PTR: printf("PTR"); break;
        case DNS_TYPE_HINFO: printf("HINFO"); break;
        case DNS_TYPE_MINFO: printf("MINFO"); break;
        case DNS_TYPE_MX: printf("MX"); break;
        case DNS_TYPE_TXT: printf("TXT"); break;
    }
}

void dns_print_class(uint16_t cls) {
    switch(cls) {
        case DNS_CLASS_IN: printf("IN"); break;
        case DNS_CLASS_CS: printf("CS"); break;
        case DNS_CLASS_CH: printf("CH"); break;
        case DNS_CLASS_HS: printf("HS"); break;
        }
}

/**
 * @brief parses the FQDN
 * 
 * @param ptr pointing to the start of the domain in the packet (! modified)
 * @return char* buffer containing the domain stringified
 */
char *dns_get_domain(u_char **ptr) {
    static char buf[512];
    memset(buf, 0, 512);
    int k, cnt;
    uint8_t len, *tmp = NULL;
    k = 0;
    len = *(*ptr)++;
    while (len) {
        if (len >= 0xc0) { // not a domain but a pointer to a previous ref
            tmp = *ptr;
            *ptr = orig + (*(*ptr - 1) & 0x3f) + **ptr;
            len = *(*ptr)++;
            continue;
        }
        cnt = 0;
        if (k) {
            buf[k++] = '.';
        }
        while (cnt < len) {
            buf[k++] = **ptr;
            (*ptr)++;
            cnt++;
        }
        len = *(*ptr)++;
    }
    if (tmp) {
        *ptr = tmp + 1;
    }
    buf[k] = '.';
    return buf;
}


void handle_dns(struct dnshdr *dns) {
    int i;
    uint16_t len;
    uint8_t *ptr;
    uint16_t type, cls;
    orig = (uint8_t *) &(dns->qid);
    // static char *opcodes[] = {
    //     "Query",
    //     "IQuery",
    //     "Status",
    //     "Unassigned",
    //     "Notify",
    //     "Update",
    //     "DSO"
    // };
    if (vflag <= V_MEDIUM) {
        printf("tid 0x%x ", ntohs(dns->qid));
        printf("%s ", dns->flags & 0x8000 ? "Response" : "Query");
        printf(
            "(%d/%d/%d/%d) ",
            ntohs(dns->nb_qst),
            ntohs(dns->nb_asr),
            ntohs(dns->nb_ath),
            ntohs(dns->nb_add)
        );
    } else {
        printf("Query ID: 0x%x\n", ntohs(dns->qid));
        printf("%s\n", dns->flags & 0x8000 ? "Response" : "Query");
        printf("Questions: %d\n", ntohs(dns->nb_qst));
        printf("Answer RRs: %d\n", ntohs(dns->nb_asr));
        printf("Authority RRs: %d\n", ntohs(dns->nb_ath));
        printf("Additional RRs: %d\n", ntohs(dns->nb_add));
    }
    ptr = dns->data;

    // questions
    if (vflag == V_FULL && ntohs(dns->nb_qst)) {
        printf("Queries:\n");
    }

    for (i = 0; i < ntohs(dns->nb_qst); i++) {
        printf("%s", dns_get_domain(&ptr));
        putsep_inline();
        type = tlv_getint(ptr, 2);
        ptr += 2;
        dns_print_type(type);
        putsep_inline();

        cls = tlv_getint(ptr, 2);
        dns_print_class(cls);
        ptr += 2; // increment one more since it was one less for getint
        putsep();

    }

    if (vflag == V_FULL && ntohs(dns->nb_asr)) {
        printf("Answers:\n");
    }

    for (i = 0; i < ntohs(dns->nb_asr); i++) {
        if (i) {
            printf("%s", vflag == V_FULL ? "\n" : ", ");
        }
        ptr += 2; // skip domain identifier
        type = tlv_getint(ptr, 2);
        dns_print_type(type);
        putsep_inline();
        ptr += 2;
        dns_print_class(tlv_getint(ptr, 2));
        ptr += 2;
        putsep_inline();
        len = tlv_getint(ptr + 4, 2);
        if (vflag >= V_MEDIUM) {
            printf("(TTL: %lu, ", tlv_getint(ptr, 4));
            printf("length: %hu) ", len);
        }
        ptr += 6;

        switch(type) {
            case DNS_TYPE_A:
                printf("%s", tlv_getip(ptr, 4));
                ptr += 4;
                break;
            case DNS_TYPE_CNAME:
                printf("%s", dns_get_domain(&ptr));
                break;
        }
    }
    printf("\n");
}