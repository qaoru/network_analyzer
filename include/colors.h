#ifndef CLS_H
#define CLS_H
#define CLS_RESET   "\033[0m"
#define CLS_BLACK   "\033[30m"      /* Black */
#define CLS_RED     "\033[31m"      /* Red */
#define CLS_GREEN   "\033[32m"      /* Green */
#define CLS_YELLOW  "\033[33m"      /* Yellow */
#define CLS_BLUE    "\033[34m"      /* Blue */
#define CLS_MAGENTA "\033[35m"      /* Magenta */
#define CLS_CYAN    "\033[36m"      /* Cyan */
#define CLS_WHITE   "\033[37m"      /* White */
#define CLS_BLACK_B   "\033[1m\033[30m"      /* Bold Black */
#define CLS_RED_B     "\033[1m\033[31m"      /* Bold Red */
#define CLS_GREEN_B   "\033[1m\033[32m"      /* Bold Green */
#define CLS_YELLOW_B  "\033[1m\033[33m"      /* Bold Yellow */
#define CLS_BLUE_B    "\033[1m\033[34m"      /* Bold Blue */
#define CLS_MAGENTA_B "\033[1m\033[35m"      /* Bold Magenta */
#define CLS_CYAN_B    "\033[1m\033[36m"      /* Bold Cyan */
#define CLS_WHITE_B   "\033[1m\033[37m"      /* Bold White */
#endif