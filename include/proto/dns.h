/**
 * @file dns.h
 * @author Paul HENG (paul.heng@etu.unistra.fr)
 * @brief 
 * @version 0.1
 * @date 2019-10-21
 * 
 * @copyright Copyright (c) 2019
 * 
 */
#ifndef PROTO_DNS_H
#define PROTO_DNS_H
#include "common.h"
#include "ethertools.h"
#include "iptools.h"

// RFC 1035 types
#define DNS_TYPE_A               1  // a host address
#define DNS_TYPE_NS              2  // an authoritative name server
#define DNS_TYPE_MD              3  // a mail destination (Obsolete - use MX)
#define DNS_TYPE_MF              4  // a mail forwarder (Obsolete - use MX)
#define DNS_TYPE_CNAME           5  // the canonical name for an alias
#define DNS_TYPE_SOA             6  // marks the start of a zone of authority
#define DNS_TYPE_MB              7  // a mailbox domain name (EXPERIMENTAL)
#define DNS_TYPE_MG              8  // a mail group member (EXPERIMENTAL)
#define DNS_TYPE_MR              9  // a mail rename domain name (EXPERIMENTAL)
#define DNS_TYPE_NULL            10 //  a null RR (EXPERIMENTAL)
#define DNS_TYPE_WKS             11 //  a well known service description
#define DNS_TYPE_PTR             12 //  a domain name pointer
#define DNS_TYPE_HINFO           13 //  host information
#define DNS_TYPE_MINFO           14 //  mailbox or mail list information
#define DNS_TYPE_MX              15 //  mail exchange
#define DNS_TYPE_TXT             16 //  text strings

// RFC 1035 classes
#define DNS_CLASS_IN              1 // the Internet
#define DNS_CLASS_CS              2 // the CSNET class (Obsolete)
#define DNS_CLASS_CH              3 // the CHAOS class
#define DNS_CLASS_HS              4 // Hesiod [Dyer 87]


struct dnshdr {
    uint16_t qid; // query ID
    uint16_t flags;
    uint16_t nb_qst; // total qustions
    uint16_t nb_asr; // total answer RRs
    uint16_t nb_ath; // total authority RRs
    uint16_t nb_add; // total additional RRs
    uint8_t  data[];
};

void handle_dns(struct dnshdr *dns);

#endif