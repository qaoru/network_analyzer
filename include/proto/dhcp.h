/**
 * @file dhcp.h
 * @author Paul HENG (paul.heng@etu.unistra.fr)
 * @brief 
 * @version 0.1
 * @date 2019-10-21
 * 
 * @copyright Copyright (c) 2019
 * 
 */
#ifndef PROTO_DHCP_H
#define PROTO_DHCP_H
#include "common.h"
#include "ethertools.h"
#include "iptools.h"
/**
 * @brief handle bootp application layer
 *
 * @param bootstrap pointer to struct bootp (cast from packet)
 */
void handle_bootp(struct bootp *bootstrap, uint16_t length);

/**
 * @brief handle bootp vendor field
 *
 * @param vend array of octets from vendor field
 */
void parse_tlv_bootp(u_int8_t *vend);

#endif