/**
 * @file common.h
 * @author Paul HENG (paul.heng@etu.unistra.fr)
 * @brief 
 * @version 0.1
 * @date 2019-10-21
 * 
 * @copyright Copyright (c) 2019
 * 
 */
#ifndef COMMON_H
#define COMMON_H
#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include <sys/time.h>
#include <time.h>
#include <unistd.h>
#include <getopt.h>
#include <string.h>
#include "colors.h"

#define BUFSIZE 256

#define V_ALWAYS 0
#define V_SHORT 1
#define V_MEDIUM 2
#define V_FULL 3

#define TABSEP vflag == V_SHORT ? '\t' : '\n'

/**
 * @brief prints message to stderr then exits
 * credits to Pierre DAVID for this function
 * 
 * @param syserr if set, perror is also called
 * @param fmt format string
 * @param ... 
 */
void error (int syserr, const char *fmt, ...);

/**
 * @brief custom printf with verbose check
 * 
 * @param threshold minimum verbose level
 * @param fmt format string
 * @param ... 
 */
void cprintf (int threshold, const char *fmt, ...);

/**
 * @brief get a readable timestamp from struct timeval
 * 
 * @param tv 
 * @return char* static buffer
 */
char *get_timestamp(const struct timeval tv);

/**
 * @brief puts a separator
 * 
 */
void putsep(void);

/**
 * @brief puts an inline separator
 * 
 */
void putsep_inline(void);

/**
 * @brief contains the program name
 * 
 */
const char *prog;

/**
 * @brief verbose flag
 * 
 */
int vflag;

#endif