/**
 * @file iptools.h
 * @author Paul HENG (paul.heng@etu.unistra.fr)
 * @brief 
 * @version 0.1
 * @date 2019-10-21
 * 
 * @copyright Copyright (c) 2019
 * 
 */
#ifndef IPTOOLS_H
#define IPTOOLS_H
#include <arpa/inet.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <netinet/ip6.h>
#include <netinet/tcp.h>
#include <netinet/udp.h>
#include <net/if_arp.h>
#include "common.h"
#include "ethertools.h"
#include "bootp.h"

/**
 * @brief get the protocol name based on protocol number
 * 
 * @param protocol
 * @return char*
 */
char *get_proto_name(int protocol);

/**
 * @brief display ip header informations
 *
 * @param packet starting at the beginning of the ip header
 */
void print_ip_info(const u_char *packet);

void print_ip6_info(const u_char *packet);

int parse_ip6_opts(int opt, const u_char *packet, int *next);

/**
 * @brief display informations about the TCP header
 *
 * @param packet starting at the beginning of the TCP header
 */
void parse_tcp(const u_char *packet, uint16_t plen);

/**
 * @brief display informations about the UDP header
 *
 * @param packet starting at the beginning of the UDP header
 */
void parse_udp(const u_char *packet);

/**
 * @brief display informations about the application layer
 *
 * @param port application port
 * @param packet starting at the end of the transport header
 */
void print_application_layer(uint16_t port, const u_char *packet, uint16_t length);

/**
 * @brief returns the string contained in a TLV section
 * 
 * @param packet pointer to the beginning of the value area
 * @param length L value of TLV
 * @return char* 
 */
char *tlv_getstring(const u_char *packet, int length);

/**
 * @brief returns the integer value of a TLV section assuming
 * network byte order
 *
 * @param packet pointer to the beginning of the value area
 * @param length L value of TLV
 * @return u_int64_t value
 */
u_int64_t tlv_getint(const u_char *packet, int length);

/**
 * @brief returns an IP address contained in a TLV section using ASCII format
 * 
 * @param packet pointer to the beginning of the value area
 * @param length L value of TLV
 * @return char* 
 */
char *tlv_getip(const u_char *packet, int length);

/**
 * @brief returns a MAC address contained in a TLV section using ASCII format
 * 
 * @param packet pointer to the beginning of the value area
 * @param length L value of TLV
 * @return char* 
 */
char *tlv_getmac(const u_char *packet);

/**
 * @brief print ARP informations
 * 
 * @param packet 
 */
void print_arp(const u_char *packet);

#ifdef __APPLE__
struct iphdr
  {
#if __BYTE_ORDER == __LITTLE_ENDIAN
    unsigned int ihl:4;
    unsigned int version:4;
#elif __BYTE_ORDER == __BIG_ENDIAN
    unsigned int version:4;
    unsigned int ihl:4;
#else
# error	"Please fix <bits/endian.h>"
#endif
    uint8_t tos;
    uint16_t tot_len;
    uint16_t id;
    uint16_t frag_off;
    uint8_t ttl;
    uint8_t protocol;
    uint16_t check;
    uint32_t saddr;
    uint32_t daddr;
    /*The options start here. */
  };
#endif

#endif