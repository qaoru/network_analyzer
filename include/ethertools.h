/**
 * @file ethertools.h
 * @author Paul HENG (paul.heng@etu.unistra.fr)
 * @brief 
 * @version 0.1
 * @date 2019-10-21
 * 
 * @copyright Copyright (c) 2019
 * 
 */
#ifndef ETHERTOOLS_H

#define ETHERTOOLS_H
#include "common.h"
#include <net/ethernet.h>
#include <netinet/if_ether.h>

#define ETH_SIZE sizeof(struct ether_header)
#define ETH_STR_SIZE 18 // 12 bytes for hexa + 5 colons + end of line

/**
 * @brief returns a mac address from a static array (ether_header)
 * 
 * @param host static array
 * @return char* 
 */
char *get_mac_addr(const u_char host[6]);

/**
 * @brief get the ethertype name
 * 
 * @param ethertype 
 * @return char* 
 */
char *get_ethertype_name(const uint16_t ethertype);

/**
 * @brief print ethernet informations
 * 
 * @param packet 
 * @param length packet length
 */
void print_ethernet_info(const u_char *packet, unsigned int length);

#endif