# General
CC			= gcc
LD			= gcc
RM			= rm -rf
RMDIR		= rmdir
INSTALL		= install

## CHANGE THIS ##
TARGET		= main
SRCDIR		= src
OBJDIR		= obj
BINDIR		= bin
## CHANGE THIS ##

# CFLAGS, LDFLAGS, CPPFLAGS, PREFIX can be overriden on CLI
CFLAGS		:= -Iinclude -g -Wall -Wextra -Werror
LDFLAGS 	:= -lpcap



# Source, Binaries, Dependencies
SRC			:= $(shell find $(SRCDIR) -type f -name '*.c')
OBJ			:= $(patsubst $(SRCDIR)/%,$(OBJDIR)/%,$(SRC:.c=.o))
DEP			:= $(OBJ:.o=.d)
BIN			:= $(BINDIR)/$(TARGET)
-include $(DEP)


# Verbosity Control, ala automake
V 			= 0


# Build Rules
.PHONY: clean
.DEFAULT_GOAL := all

all: setup $(BIN)
setup: dir
remake: clean all

dir:
	@mkdir -p $(OBJDIR)
	@mkdir -p $(BINDIR)


$(BIN): $(OBJ)
	$(LD) $(LDFLAGS) $^ -o $@

$(OBJDIR)/%.o: $(SRCDIR)/%.c
	$(CC) $(CFLAGS) $(CFLAGS) -c -o $@ $<

clean:
	$(RM) $(OBJ) $(DEP) $(BIN)
	$(RMDIR) $(OBJDIR) $(BINDIR)

clean_all: clean
	$(RM) html

doc:
	doxygen

